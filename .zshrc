#!/usr/bin/env zsh
: <<'!COMMENT'

Louis T. Getterman IV : Dotfiles
https://LTG.FYI/Dotfiles

Inspired by:

Chewrocca : dotfiles
https://github.com/chewrocca/dotfiles

Anand Iyer : A simpler way to manage your dotfiles
https://www.anand-iyer.com/blog/2018/a-simpler-way-to-manage-your-dotfiles.html

!COMMENT

#------------------------------------------------------------------------------
# 1. Script Variables
#------------------------------------------------------------------------------

# Preferred terminal multiplexer
# (ideally this would go in .env)
TERMIPLEXER="byobu"

# Temporary directory
# https://unix.stackexchange.com/questions/352107/generic-way-to-get-temp-path
TMPDIR="${TMPDIR:-/tmp}"

# SSH binary path
ORIGSSHPATH=`which ssh`

# Determine kernel and operating system
case "$OSTYPE" in
	linux*)
		KERNEL="Linux"
		DISTROID=`lsb_release --id | awk -F"\t" '{print $2}'`
		DISTROVER=`lsb_release --release | awk -F"\t" '{print $2}'`
		;;
	darwin*)
		KERNEL="Darwin"
		DISTROID="macOS"
		DISTROVER=`defaults read loginwindow SystemVersionStampAsString`
		;;
	win*)
		KERNEL="Windows"
		;;
	msys*)
		KERNEL="MinGW"
		;;
	cygwin*)
		KERNEL="Cygwin"
		;;
	bsd*)
		KERNEL="BSD"
		;;
	solaris*)
		KERNEL="Solaris"
		;;
	*)
		KERNEL="Unknown ($OSTYPE)"
		DISTROID="Unknown ($OSTYPE)"
		DISTROVER="Unknown ($OSTYPE)"
		;;
esac

# Determine if working remote or not
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ] || [ -n "$SSH_CONNECTION" ]; then
	IAMREMOTE=1
else
	IAMREMOTE=0
fi

#------------------------------------------------------------------------------
# 2. Terminal multiplexers
#------------------------------------------------------------------------------

case "$TERMIPLEXER" in

	tmux)
		if [ -z "$TMUX" ]; then
			tmux attach -t TMUX || tmux new -s TMUX
		fi
		;;

	screen)
		if [ -z "$STY" ]; then
			screen -rd shell 2>&1 >/dev/null && exit || screen -SU shell && exit
		fi
		;;

	byobu)
		if [ -z "$BYOBU_TTY" ] && [ "$IAMREMOTE" -ne 1 ]; then
			_byobu_sourced=1 . byobu-launch 2>/dev/null || true
		fi
		;;

	# None
	*)
		;;

esac

#------------------------------------------------------------------------------
# 3. Powerlevel10k
#------------------------------------------------------------------------------

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
	source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

#------------------------------------------------------------------------------
# 4. Pre-flight setup
#------------------------------------------------------------------------------

: <<'!COMMENT'
Check for required commands:
	https://www.zsh.org/
	https://stedolan.github.io/jq/
!COMMENT
for currCmd in \
	\
	'zsh' \
	'jq' \
	\
	; do
	if ! command -v "${currCmd}" >/dev/null 2>&1; then
		>&2 echo "${DISTROID} command not found: '${currCmd}'. Exiting ZSH setup."
		return 1
	fi
done
unset currCmd

# Check for shell
if [ ! `basename ${SHELL}` = 'zsh' ]; then

	printf -- '-%.0s' {1..100}; echo ""

	echo "Your shell is set to $(basename $SHELL), not zsh."

	chshCmd=
	case "$KERNEL" in

		Linux)
			chshCmd="usermod --shell $(which zsh) $(whoami)"
			;;

		Darwin)
			chshCmd="chsh -s $(which zsh) $(whoami)"

			if ! grep -q zsh /etc/shells; then
				echo 'You will also need to add your shell to /etc/shells.  See https://LTG.FYI/macos-shells for more information.'
			fi
			;;

	esac

	# Update user shell
	if [ -n "${chshCmd}" ]; then
		echo -n "Set ZSH as your default shell? [y/N]: "
		if read -q; then
			echo;
			echo -n "Setting your default shell to ZSH... "
			eval "(${chshCmd}) 2>/dev/null"
			echo -n "Done!"
		fi
		echo;
	fi

	printf -- '-%.0s' {1..100}; echo ""

fi

# Install zplug (https://github.com/zplug/zplug) if it's not already here.
# Plug-ins available at https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/
if [ -d ~/.zplug/ ]; then
	source ~/.zplug/init.zsh
else
	curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
	>&2 echo "zplug has been installed.  Please restart your shell to take effect."
	return
fi

#------------------------------------------------------------------------------
# 5. Exports
#------------------------------------------------------------------------------

# Base
export PATH="/usr/local/bin:/usr/local/sbin:$PATH"

# Rust — this isn't needed since we source Rust below
# export PATH="$HOME/.cargo/bin:$PATH"

if [ "${DISTROID}" = "macOS" ]; then

	# Homebrew
	export PATH="/usr/local/opt/sqlite/bin:/usr/local/opt/coreutils/libexec/gnubin:$PATH"
	export BYOBU_PREFIX=$(brew --prefix)

	# Visual Studio Code — https://code.visualstudio.com/
	export PATH="/Applications/Visual Studio Code.app/Contents/Resources/app/bin:$PATH"

fi

# Sublime
if [ "${DISTROID}" = "macOS" ]; then
	export EDITOR="sublime --wait"

# 🖕 I don't care what you say, Chewrocca (and Alfie!), I love Nano! 🖕
elif command -v nano >/dev/null 2>&1; then
	export EDITOR="nano"

fi

# UTF-8
# If problems persist, try https://askubuntu.com/a/162394:
# sudo -s -- 'locale-gen en_US en_US.UTF-8; dpkg-reconfigure locales;'
# If the problem still persists, try `apt-get install language-pack-en-base`
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

# Session ID
export SHELLSESSDIR=`mktemp --directory`
chmod 700 "${SHELLSESSDIR}"

# 1Password session
# export OPSESSPATH="${SHELLSESSDIR}/opsessionid"
export OPSESSPATH="${TMPDIR}/opsessionid"

# Go language — https://golang.org/doc/install
if command -v go >/dev/null 2>&1; then
	export GOPATH="$HOME/golang"
	export PATH="$PATH:$(go env GOPATH)/bin"
# else
# 	brew install go
# 	snap install --classic go
fi

# Network interfaces as exported variables
# Kelsey Hightower's setup-network-environment
# if command -v "setup-network-environment" >/dev/null 2>&1 && [ ! -f "${TMPDIR}/network-environment" ]; then
if command -v "setup-network-environment" >/dev/null 2>&1 && [ -f "/etc/network-environment" ]; then
	# setup-network-environment -o "${TMPDIR}/network-environment" >/dev/null 2>&1
	# sed -i'' 's~[^0-9A-Za-z\=\.\_/]\+~_~g' "${TMPDIR}/network-environment"
# elif command -v go >/dev/null 2>&1; then
# 	go get github.com/kelseyhightower/setup-network-environment
	source "/etc/network-environment"
fi
# if [ -f "${TMPDIR}/network-environment" ]; then
# 	source "${TMPDIR}/network-environment"
# fi

# Removes inverted % signs when a new line is missing.
export PROMPT_EOL_MARK=''

# Rust Programming Language
if [ -f "$HOME/.cargo/env" ]; then

	source "$HOME/.cargo/env"

	# rustup completions -> rustup completions zsh cargo
	#compdef cargo
	# source $(rustc --print sysroot)/share/zsh/site-functions/_cargo

	zplug "plugins/rust",			from:oh-my-zsh
	zplug "plugins/rustup",			from:oh-my-zsh

fi

# Brew auto-updates everything when you install unrelated packages.
# https://github.com/Homebrew/brew/issues/1670
export HOMEBREW_NO_AUTO_UPDATE=1

#------------------------------------------------------------------------------
# 6. Aliases
#------------------------------------------------------------------------------

# Paths
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

# Listing
# https://github.com/ogham/exa
if command -v exa >/dev/null 2>&1; then
	alias ll="exa --long --grid --across --all"
else
	alias ll="ls -al --color=always"
fi

# Download
# https://askubuntu.com/questions/758496/how-to-make-a-permanent-alias-in-oh-my-zsh
if command -v "youtube-dl" >/dev/null 2>&1; then
	alias ytdl="youtube-dl -o '%(id)s.%(ext)s'"
fi

# Versioning
alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# Hierarchical listing
if command -v tree >/dev/null 2>&1; then
	alias tree="tree -a -I '.git'"
fi

# Better process viewing, with a privileged view.
if command -v htop >/dev/null 2>&1; then
	alias top="sudo htop"
fi

# Used for loading SSH keys on demand (and because I always forget to use Mosh)
alias ssh="connectShell mosh $@"
alias secureshell="connectShell ssh $@"

# Seedboxes quota
if [ "$IAMREMOTE" -ne 1 ]; then
	alias "seedboxes-quota"="secureshell seedboxes-quota 2>/dev/null"
fi

# Base64 encoding and decoding
alias b64e="base64 --wrap=0"
alias b64d="base64 --decode"

# Clear scroll history (available with F7)
# https://askubuntu.com/questions/923320/how-to-clear-history-in-byobu
alias "byobu-clear-history"="clear && byobu clear-history"

# WTF is my IP?! — https://wtfismyip.com/
alias "wtfismyip"="curl --ipv4 https://wtfismyip.com/text; curl --ipv6 https://wtfismyip.com/text"

# Copy key to remote host
alias "sshCopyKey"="ssh-copy-id"

# Remove known SSH host
alias "sshRemoveHost"="ssh-keygen -R"

# Display QR code in terminal
if command -v qrencode >/dev/null 2>&1; then
	alias "qr"="qrencode -t ansiutf8"
fi

#------------------------------------------------------------------------------
# 7. Colors
#------------------------------------------------------------------------------

autoload -U colors && colors
export CLICOLOR="yes"

# Syntax highlighting for commands, load last
zplug "zsh-users/zsh-syntax-highlighting",	from:github, defer:3

zplug "zsh-users/zsh-syntax-highlighting",	defer:2

zplug "plugins/colored-man-pages",			from:oh-my-zsh

#------------------------------------------------------------------------------
# 8. Completion
#------------------------------------------------------------------------------

# Load completion library for those sweet [tab] squares
zplug "lib/completion",						from:oh-my-zsh

zplug "zsh-users/zsh-autosuggestions",		defer:2
zplug "zsh-users/zsh-completions",			defer:2

#------------------------------------------------------------------------------
# 9. History
#------------------------------------------------------------------------------
if [ -z $HISTFILE ]; then
	HISTFILE=$HOME/.zsh_history
fi
HISTSIZE=10000
SAVEHIST=10000

setopt append_history
setopt inc_append_history
setopt extended_history
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_verify
setopt share_history

#------------------------------------------------------------------------------
# 10. Prompt
#------------------------------------------------------------------------------

# https://www.reddit.com/r/zsh/comments/mmu87a/what_plugin_gives_you_the_functionality_of_did/
setopt correct

# zplug 'dracula/zsh', as:theme

# Async for zsh, used by pure
# zplug "mafredri/zsh-async", from:"github", use:"async.zsh" defer:0
# zplug "sindresorhus/pure", use:pure.zsh, from:github, as:theme

# https://github.com/romkatv/powerlevel10k
zplug "romkatv/powerlevel10k",				as:theme, depth:1

#------------------------------------------------------------------------------
# 11. Functions
#------------------------------------------------------------------------------

##-------------------------------------------------------------------
## myIP address
## -------------------------------------------------------------------
function myip() {

	# IMHO, it would be better to use `setup-network-environment`
	# https://github.com/kelseyhightower/setup-network-environment

	case "$DISTROID" in

		macOS)
			ifconfig lo0 | grep 'inet ' | sed -e 's/:/ /' | awk '{print "lo0       : " $2}'
			ifconfig en0 | grep 'inet ' | sed -e 's/:/ /' | awk '{print "en0 (IPv4): " $2 " " $3 " " $4 " " $5 " " $6}'
			ifconfig en0 | grep 'inet6 ' | sed -e 's/ / /' | awk '{print "en0 (IPv6): " $2 " " $3 " " $4 " " $5 " " $6}'
			ifconfig en1 | grep 'inet ' | sed -e 's/:/ /' | awk '{print "en1 (IPv4): " $2 " " $3 " " $4 " " $5 " " $6}'
			ifconfig en1 | grep 'inet6 ' | sed -e 's/ / /' | awk '{print "en1 (IPv6): " $2 " " $3 " " $4 " " $5 " " $6}'
			;;

	esac

}

# -------------------------------------------------------------------
# compressed file expander
# (from https://github.com/myfreeweb/zshuery/blob/master/zshuery.sh)
# -------------------------------------------------------------------
function ex() {
	if [[ -f $1 ]]; then
		case $1 in
		  *.tar.bz2) tar xvjf $1;;
		  *.tar.gz) tar xvzf $1;;
		  *.tar.xz) tar xvJf $1;;
		  *.tar.lzma) tar --lzma xvf $1;;
		  *.bz2) bunzip $1;;
		  *.rar) unrar $1;;
		  *.gz) gunzip $1;;
		  *.tar) tar xvf $1;;
		  *.tbz2) tar xvjf $1;;
		  *.tgz) tar xvzf $1;;
		  *.zip) unzip $1;;
		  *.Z) uncompress $1;;
		  *.7z) 7z x $1;;
		  *.dmg) hdiutul mount $1;; # mount OS X disk images
		  *) echo "'$1' cannot be extracted via >ex<";;
	esac
	else
		echo "'$1' is not a valid file"
	fi
}

# -------------------------------------------------------------------
# Echo dash lines
# -------------------------------------------------------------------
function str_repeat() {
	local  char=$1
	local  repeat=$2

	local  stat="`seq -s${char} ${repeat} | tr -d '[:digit:]'`"

	echo $stat
} # END FUNCTION: str_repeat

# -------------------------------------------------------------------
# Change PDF title
# pdftitle hello.pdf "Hello, World!"
# -------------------------------------------------------------------
if command -v exiftool >/dev/null 2>&1; then

	function pdftitle() {
		exiftool -Title="$2" "$1"
		return $?
	}

fi

# -------------------------------------------------------------------
# Generate an SSH key pair
# https://medium.com/risan/upgrade-your-ssh-key-to-ed25519-c6e8d60d3c54
# https://blog.filippo.io/using-ed25519-keys-for-encryption/
# -------------------------------------------------------------------
function sshNewKeyPair() {

	# E.g. dsa, ecdsa, ed25519, or rsa
	local keyType=${1-'ed25519'}

	# E.g. b64e
	local keyEncoding=${2-'cat'}

	str_repeat - 80
	echo;

	local tempgenkey="$(mktemp)"

	# Something went wrong, abort.
	if [ "$?" -ne 0 ]; then
		return 1
	fi

	# Status message
	echo -n "Generating '${keyType}' key pair with "
	if [ "${keyEncoding}" = "cat" ]; then
		echo -n "no"
	else
		echo -n "'${keyEncoding}'"
	fi
	echo " encoding.\n"

	# The first option messes with Sublime's syntax highlighting. 😓
	# So I use the second option instead. 🙄
	# Pick whichever one that you prefer.
	# ssh-keygen -q -o -a 100 -t "${keyType}" -N '' -f "${tempgenkey}" -C "${NAME_SHORT}" <<<y 2>&1 >/dev/null
	echo 'y' | ssh-keygen -q -o -a 100 -t "${keyType}" -N '' -f "${tempgenkey}" -C "${NAME_SHORT}" 2>&1 >/dev/null

	echo "Public key:"
	echo;
	eval "cat ${tempgenkey}.pub | ${keyEncoding}"
	rm -rf "${tempgenkey}.pub"

	echo;
	str_repeat - 40
	echo;

	echo "Private key:"
	echo;
	eval "cat ${tempgenkey} | ${keyEncoding}"
	rm -rf "${tempgenkey}"

	echo;
	str_repeat - 80

}

# -------------------------------------------------------------------
# List loaded keys from SSH agent
# -------------------------------------------------------------------
function loadsshkeys() {

	if command -v op >/dev/null 2>&1; then
		oploadsshkeys
	fi

}

# -------------------------------------------------------------------
# List loaded keys from SSH agent
# -------------------------------------------------------------------
function loadedsshkeys() {
	local r=`ssh-add -l`
	rc="$?"

	if [ "$r" = "The agent has no identities." ]; then
		echo ""
		return 1
	else
		echo "${r}"
		return $rc
	fi

}

# -------------------------------------------------------------------
# Unload keys from SSH agent
# -------------------------------------------------------------------
function unloadsshkeys() {
	ssh-add -D >/dev/null 2>&1
}

# -------------------------------------------------------------------
# Used for removing temporary directory created at start of shell.
# -------------------------------------------------------------------
function cleanup {
	rm -rf "${SHELLSESSDIR}"
}
trap cleanup EXIT

# -------------------------------------------------------------------
# Used as a wrapper for Mosh and SSH, and to load SSH keys when needed.
# -------------------------------------------------------------------
function connectShell() {

	local connectionType="$1"
	local connectionArgs="${@:2}"

	if ! command -v mosh >/dev/null 2>&1 && [ "${connectionType}" = "mosh" ]; then
		connectionType="ssh"
	fi

	# Load SSH keys
	if [ -z "$(loadedsshkeys)" ]; then
		>&2 echo "No keys are loaded.  Run 'loadsshkeys' if you wish to load keys."
	fi

	case "${connectionType}" in

		mosh)
			eval mosh "${connectionArgs}"
			;;

		ssh)
			eval "${ORIGSSHPATH}" "${connectionArgs}"
			;;

	esac

}


# -------------------------------------------------------------------
# Create bcrypt-based hash of a string
# https://unix.stackexchange.com/a/419855
# htpasswd is typically found in apache2-utils
# -------------------------------------------------------------------
function bcrypt() {

	# Escape $ with another one
	# https://stackoverflow.com/questions/49484916/trafik-io-as-docker-container-with-basic-auth
	# echo $(htpasswd -nb <AUTH-USER> <AUTH-PASS>) | sed -e s/\\$/\\$\\$/g

	if [ -z "$1" ]; then
		echo -n "Password to hash with bcrypt: "
		read -s password
		echo;
	else
		password="$1"
	fi

	htpasswd -bnBC 12 "" $password | tr -d ':\n' | sed 's/$2y/$2a/'

}

# -------------------------------------------------------------------
# SSH host fingerprints
# https://winscp.net/eng/docs/faq_hostkey
# -------------------------------------------------------------------
function sshServerFingerPrints() {
	for f in /etc/ssh/ssh_host_*_key; do ssh-keygen -l -f "$f"; done
}

#------------------------------------------------------------------------------
# 12. FASD & FZF
# - https://github.com/clvv/fasd
# - https://github.com/junegunn/fzf
#------------------------------------------------------------------------------

# https://github.com/junegunn/fzf
if command -v fzf >/dev/null 2>&1; then

	# https://github.com/Aloxaf/fzf-tab
	zplug "Aloxaf/fzf-tab",						defer:3

	# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/fzf
	zplug "plugins/fzf",						from:oh-my-zsh

fi

# https://github.com/clvv/fasd
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/fasd
if command -v fasd >/dev/null 2>&1; then
	zplug "plugins/fasd",						from:oh-my-zsh
fi

#------------------------------------------------------------------------------
# 13. Development
#------------------------------------------------------------------------------

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/docker
if command -v docker >/dev/null 2>&1; then
	zplug "plugins/docker",						from:oh-my-zsh
fi

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/docker-compose
if command -v docker-compose >/dev/null 2>&1; then
	zplug "plugins/docker-compose",				from:oh-my-zsh
fi

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/git
if command -v git >/dev/null 2>&1; then
	zplug "plugins/git",						from:oh-my-zsh
fi

# pyenv
# https://github.com/pyenv/pyenv
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/pyenv
if [ -d "${HOME}/.pyenv" ]; then

	export PYENV_VIRTUALENV_DISABLE_PROMPT=1
	export PYENV_ROOT="$HOME/.pyenv"
	export PATH="$PYENV_ROOT/bin:$PATH"
	eval "$(pyenv init --path)"

	zplug "plugins/pyenv",						from:oh-my-zsh,		defer:2

fi

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/sublime
if [ command -v sublime >/dev/null 2>&1 ] || [ command -v subl >/dev/null 2>&1 ]; then
	zplug "plugins/sublime",					from:oh-my-zsh
fi

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/vagrant
if command -v vagrant >/dev/null 2>&1; then
	zplug "plugins/vagrant",					from:oh-my-zsh
fi

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/xcode
if command -v xcodebuild >/dev/null 2>&1; then
	zplug "plugins/xcode",						from:oh-my-zsh
fi

#------------------------------------------------------------------------------
# 14. Administrative
#------------------------------------------------------------------------------

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/ansible
zplug	"plugins/ansible",				from:oh-my-zsh

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/doctl
if command -v doctl >/dev/null 2>&1; then
	zplug	"plugins/doctl",				from:oh-my-zsh
fi

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/terraform
if command -v terraform >/dev/null 2>&1; then
	zplug	"plugins/terraform",			from:oh-my-zsh
fi

#------------------------------------------------------------------------------
# 15. Console
#------------------------------------------------------------------------------

# https://github.com/zsh-users/zsh-history-substring-search
zplug "zsh-users/zsh-history-substring-search"

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/dotenv
# $ZSH_CACHE_DIR/dotenv-allowed.list
# $ZSH_CACHE_DIR/dotenv-disallowed.list
zplug "plugins/dotenv",				from:oh-my-zsh

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/safe-paste
zplug "plugins/safe-paste",			from:oh-my-zsh

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/tmux
# zplug "plugins/tmux",				from:oh-my-zsh

# "Oh My Zsh" library : miscellaneous : escape sequences for paste
zplug "ohmyzsh/ohmyzsh",			as:plugin, use:"lib/misc.zsh"

# macOS : ZSH
if [ "${DISTROID}" = "macOS" ]; then
	bindkey "^[[H" beginning-of-line
	bindkey "^[[F" end-of-line
fi

#------------------------------------------------------------------------------
# 16. System
#------------------------------------------------------------------------------

if [ "${DISTROID}" = "macOS" ]; then

	# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/brew
	zplug "plugins/brew",					from:oh-my-zsh

	# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/osx
	zplug "plugins/osx",					from:oh-my-zsh

	# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/iterm2
	zplug "plugins/iterm2",					from:oh-my-zsh

	# Terrible workaround for `fasd` missing my defined alias.
	if [ ! -L "/usr/local/bin/sublime" ]; then
		ln -s "/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl" "/usr/local/bin/sublime"
	fi

fi

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/fancy-ctrl-z
# See http://sheerun.net/2014/03/21/how-to-boost-your-vim-productivity/
zplug "plugins/fancy-ctrl-z",				from:oh-my-zsh

#------------------------------------------------------------------------------
# 17. Communications
#------------------------------------------------------------------------------

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/rsync
if command -v rsync >/dev/null 2>&1; then
	zplug "plugins/rsync",						from:oh-my-zsh
fi

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/torrent
zplug "plugins/torrent",					from:oh-my-zsh

#------------------------------------------------------------------------------
# 18. Research
#------------------------------------------------------------------------------

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/nmap
if command -v nmap >/dev/null 2>&1; then
	zplug "plugins/nmap",						from:oh-my-zsh
fi

#------------------------------------------------------------------------------
# 19. 1Password — thanks to Chewrocca for the discussion and help.
# Additional references:
# https://support.1password.com/command-line/
# https://grantorchard.com/securing-environment-variables-with-1password/
#------------------------------------------------------------------------------

if command -v op >/dev/null 2>&1; then

	# 1Password completion
	eval "$(op completion zsh)"; compdef _op op

	# Enable 1Password
	opon() {

		# Test for active session
		if [ -f "${OPSESSPATH}" ]; then

			op get user "${OP_USERNAME}" --session "$(cat $OPSESSPATH)" >/dev/null 2>&1
			if [ $? -ne 0 ]; then
				opoff
			fi

		fi

		if [ -z "$OP_SESSION_personal" ] && [ -f "${OPSESSPATH}" ]; then

			# Load from cache file
			export OP_SESSION_personal="$(cat ${OPSESSPATH})"

		elif [ -z "$OP_SESSION_personal" ]; then

			# Sign-in
			if [ -f "${OPSESSPATH}" ]; then
				eval $(op signin ${OP_SERVER} ${OP_USERNAME} ${OP_SECRET} --shorthand personal --session "$(cat $OPSESSPATH)")
			else
				eval $(op signin ${OP_SERVER} ${OP_USERNAME} ${OP_SECRET} --shorthand personal)
			fi

			# Save session — environment variables between functions is problematic due to sub-shells.
			echo -n "${OP_SESSION_personal}" > "${OPSESSPATH}"
			chmod 600 "${OPSESSPATH}"

		fi

		echo "${OP_SESSION_personal}"
	}

	# Disable 1Password
	opoff() {
		op signout
		unset OP_SESSION_personal
		rm -rf "${OPSESSPATH}"
	}

	# Get Password from 1Password for item's Name or UUID
	oppass() {

		# Sign-in
		opon >/dev/null

		# JSON sometimes ends in a trailing character (incompatibility with p10k?)
		op get item "$1" --fields password

	}

	# Load a note from 1Password
	opnote() {

		# Sign-in
		opon >/dev/null

		# JSON sometimes ends in a trailing character (incompatibility with p10k?)
		op get item "$1" --fields notesPlain

	}

	# Retrieve Base64 encoded SSH key from 1Password where argument is name of item (e.g. id_ed25519)
	opsshkey() {

		local opgi=""
		local oprc=1

		# Forgot argument
		if [ -z "$1" ]; then
			echo ""
			return 1
		fi

		# Sign-in
		opon >/dev/null

		opgi=`op get item "$1" --fields private`
		oprc=$?

		# Success, proceed with Base64 decoding
		if [ "${oprc}" -eq 0 ]; then
			echo "${opgi}" | b64d
		fi

		return $oprc

	}

	# Specific SSH key to load
	oploadsshkey() {

		local opkeyr=""
		local opkeyrc=1

		# Forgot argument
		if [ -z "$1" ]; then
			echo ""
			return 1
		fi

		opkeyr="$(opsshkey "$1")"
		opkeyrc=$?

		# Success, store key for configured time.
		if [ "${opkeyrc}" -eq 0 ]; then
			echo "${opkeyr}" | ssh-add -t ${OP_SSHKEYLENGTH} -
		fi

	}

	# Load (and decode) a Base64-encoded value from 1Password
	oploadsshkeys() {

		local keyType

		IFS=' ' read -A keyType <<<"$OP_SSHKEYS"
		for i in "${keyType[@]}"; do
			oploadsshkey "$i"
		done

		unset keyType

	}

fi

#------------------------------------------------------------------------------
# 20. Initialization
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then

	zplug install

	# Abort p10k
	>&2 echo "zplugins installed, please restart your shell."
	return

fi
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Then, source plugins and add commands to $PATH
zplug load
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
#------------------------------------------------------------------------------
